# Creating a Maven project
mvn.sh archetype:generate -DgroupId=siriusXM -DartifactId=VoiceTester -DarchetypeArtifactId=maven-archetype-quickstart -DarchetypeVersion=1.4 -DinteractiveMode=false

# HTTP Client
https://hc.apache.org/httpcomponents-client-ga/index.html

Apache HTTP client is pretty wonky.

Using Unirest instead - works better :-)

# Jenkins
Used Bitnami to setup Jenkins instance.

Guide for integrating Jenkins with Github:
https://medium.com/@shreyaklexheal/integrate-jenkins-with-github-private-repo-8fb335494f7e

Adding SSL for Jenkins:
https://docs.bitnami.com/general/apps/jenkins/administration/enable-https-ssl-apache/

# Reporting
https://maven.apache.org/surefire/maven-surefire-report-plugin/index.html

Hosted grafana/graphite:
https://grafana.com/products/cloud/

# AppDynamics
Looks like we want to use AppDynamics Analytics:
https://docs.appdynamics.com/display/PRO45/Analytics+Events+API

It appears I need a special license for this to work:
https://bespoken.saas.appdynamics.com/controller/#/location=LICENSE_MANAGEMENT_PEAK_USAGE&timeRange=last_1_hour.BEFORE_NOW.-1.-1.60

curl -X POST \
  https://analytics.api.appdynamics.com \
  -H 'Content-Type: application/vnd.appd.events+json;v=2' \
  -H 'Postman-Token: e814de21-3c33-4926-b69e-da54dc7a69e1' \
  -H 'X-Events-API-AccountName: 479599-ss-Bespoken-wd9ek3did2y1' \
  -H 'X-Events-API-Key: eyJraWQiOiIxIiwiYWxnIjoiSFMyNTYifQ.eyJpc3MiOiJBcHBEeW5hbWljcyIsImF1ZCI6IkFwcERfQVBJcyIsImp0aSI6IndlUVJldm1oR0tRVThJVFpwSGFpalEiLCJzdWIiOiJTdGF0aW9uVGVzdCIsImlkVHlwZSI6IkFQSV9DTElFTlQiLCJpZCI6ImQ4NTUxNGY3LTRjNDctNDJmZi1iMTIxLWEyMDBkODljMGMyYyIsImFjY3RJZCI6ImIyOGU0ZDQzLTU3NzAtNGRmNS04Y2FjLTI1ZGE1Y2E1ZDNlNyIsImFjY3ROYW1lIjoiYmVzcG9rZW4iLCJ0ZW5hbnROYW1lIjoiIiwiaWF0IjoxNTc0MTE4OTM1LCJuYmYiOjE1NzQxMTg4MTUsImV4cCI6MTYwNTY1NDkzNSwidG9rZW5UeXBlIjoiQUNDRVNTIn0.mdO4S5epV1W_D-GZGPIRzAEi1ZamQ_xfKsFspu0HBGk' \
  -H 'cache-control: no-cache'