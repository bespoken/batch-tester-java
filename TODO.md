## High Priority
- [ ] Use environment variables for virtual devices
- [ ] Hook into AppDynamics
- [ ] Add more virtual device configuration options

# Interesting
- [ ] Use Github plugin for Jenkins instead of polling?
  - [ ] https://wiki.jenkins.io/display/JENKINS/GitHub+Plugin
- [ ] Add nice domain name and SSL for our Jenkins instance

# Completed
- [X] Figure out how to use CSV headers
- [X] Hook into Jenkins 
  - [X] Is there a hosted jenkins available?

