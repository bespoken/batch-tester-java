package bespoken;

import java.math.BigDecimal;

import kong.unirest.*;
import kong.unirest.json.*;

// Manages interactions with the Bespoken Virtual Device API
public class VirtualDevice {
  public static class Configuration {
    public String baseURL;
    public String token;
    public boolean debug;
    public BigDecimal latitude;
    public BigDecimal longitude;

    public Configuration() {
  
    }
  
    // Sets the baseURL for the virtual device
    public Configuration baseURL(String baseURL) {
      this.baseURL = baseURL;
      return this;
    }
    
    // Toggles whether the full payload should be included in the response
    public Configuration debug(boolean debug) {
      this.debug = debug;
      return this;
    }
    
    // Sets the token for the virtual device
    public Configuration token(String token) {
      this.token = token;
      return this;
    }

    public Configuration location(BigDecimal latitude, BigDecimal longitude) {
      this.latitude = latitude;
      this.longitude = longitude;
      return this;
    }
    
  }

  private Configuration configuration;
  public VirtualDevice(Configuration configuration) {
    this.configuration = configuration;
  }

  // Sends a single message to the Bespoken Virtual Device
  public JSONObject message (String message) {
    try {
      return this.messageImpl(message, 0);
    } catch (Exception e) {
      return new JSONObject();
    }
  }

  private JSONObject messageImpl(String message, int retry) {
    //Unirest.post("https://httpbin.org/post");
    String url = this.configuration.baseURL + "/process";
    System.out.println("URL: " + url);
    GetRequest request = Unirest.get(url)
      .header("accept", "application/json");
    if (this.configuration.token != null) {
      request.queryString("user_id", this.configuration.token);
    }

    if (this.configuration.debug) {
      request.queryString("debug", "true");
    }

    if (this.configuration.latitude != null) {
      request.queryString("location_lat", this.configuration.latitude);
      request.queryString("location_long", this.configuration.longitude);
    }

    request.queryString("message", message);
    HttpResponse<JsonNode> response = request.asJson();
    System.out.println("Response Status: " + response.getStatus());
    if (response.getStatus() != 200) {
      if (retry < 3) {
        System.out.printf("Retrying - count: %d\n", retry);
        return this.messageImpl(message, retry + 1);
      }

      System.out.println("JSON: " + response.getBody().getObject().toString());
      throw new RuntimeException(response.getStatusText());
    }
    return response.getBody().getObject();
  }
}

