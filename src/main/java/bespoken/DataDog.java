package bespoken;

import java.util.*;
import kong.unirest.*;

// Manages sending metrics to DataDog
public class DataDog {
  private final String apiKey;

  public DataDog(final String apiKey) {
    this.apiKey = apiKey;
  }

  // Posts metrics using the DataDog HTTP API
  // Docs are here: https://docs.datadoghq.com/api/?lang=bash#post-timeseries-points
  public void postMetrics(final Metric [] metrics) {
    final String url = "https://api.datadoghq.com/api/v1/series?api_key=" + this.apiKey;
    HashMap<String, Metric[]> series = new HashMap<String, Metric []>();
    series.put("series", metrics);
    GsonObjectMapper mapper = new GsonObjectMapper();
    String metricString = mapper.writeValue(series);
    
    final HttpResponse<JsonNode> response = Unirest.post(url)
      .header("Content-Type", "application/json")
      .body(metricString)
      .asJson();
    
    if (response.getStatus() != 202) {
      System.err.println("ERROR! Failed writing data to DataDog: " + response.getStatusText());
    }
    
  }

  // This class and the MetricPoint class are automatically serialized to JSON to send to DataDog
  public static class Metric {
    public String metric;
    public String type;
    public String interval;
    public ArrayList<Long []> points = new ArrayList<Long []>();
    public List<String> tags = new ArrayList<String>();

    public Metric(String metric, String type, Long value, String [] tags) {
      this.metric = metric;
      this.type = type;
      this.tags = Arrays.asList(tags);
      this.addPoint(value);
    }

    public void addPoint(final Long value) {
      this.points.add(new MetricPoint(value).toArray());
    }

    public void addTag(final String tag) {
      this.tags.add(tag);
    }
  }

  public static class MetricPoint {
    public Long timestamp;
    public Long value;

    public MetricPoint(Long value) {
      this.value = value;
      this.timestamp = System.currentTimeMillis() / 1000;
    }

    public Long[] toArray() {
      return new Long [] {this.timestamp, this.value};
    }
  }
}