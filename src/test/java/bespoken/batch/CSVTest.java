package bespoken.batch;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;

import bespoken.*;
import com.jayway.jsonpath.*;
import io.github.cdimascio.dotenv.Dotenv;
import java.io.*;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import kong.unirest.json.*;
import org.apache.commons.csv.*;
import org.junit.jupiter.api.*;

/**
 * Runs through a series of tests:
 * - Loads a bunch of station names from a CSV
 * - Requests each station from Alexa
 * - Compares the response from Alexa (including the stream URL and image URL) to the expected response in the CSV
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CSVTest 
{
    public static Dotenv dotenv;
    public List<OutputRecord> outputRecords = new ArrayList<OutputRecord>();
    private String runName = new SimpleDateFormat("yyyy-MM-dd_HH:mm").format(new Date());
    @BeforeAll
    public void beforeAll() {
        dotenv = Dotenv.configure().ignoreIfMissing().load();
    }

    /** 
     * Turns the CSV file into Dynamic Tests
     */
    @TestFactory
    Collection<DynamicTest> csvTests() throws Exception {
        String inputPath = System.getProperty("user.dir") + "/inputs.csv";
        System.out.println("CSV: " + inputPath);
        Reader in = new FileReader(inputPath);
        Iterable<CSVRecord> records = CSVFormat.EXCEL.withFirstRecordAsHeader().parse(in);
        ArrayList<DynamicTest> tests = new ArrayList<DynamicTest>();

        // Run a test for each input record from the CSV
        for (CSVRecord record : records) {
            DynamicTest test = DynamicTest.dynamicTest(record.get("entity"), () -> this.invoke(record));
            tests.add(test);
        }

        return tests;
    }

    @AfterAll
    public void afterAll() {
        this.printResults();
    }

    /**
     * Calls the virtual device with the info from the CSV record
     * Saves the result as an output record
     * @param inputRecord
     */
    private void invoke(CSVRecord inputRecord) {
        VirtualDevice.Configuration config = new VirtualDevice.Configuration()
            .baseURL("https://virtual-device.bespoken.io")
            .debug(true)
            .location(new BigDecimal("38.9072"), new BigDecimal("77.0369"))
            .token(dotenv.get("VIRTUAL_DEVICE_TOKEN"));
        System.out.println("TOKEN: " + dotenv.get("VIRTUAL_DEVICE_TOKEN"));
        VirtualDevice vd = new VirtualDevice(config);
        final String station = inputRecord.get("entity").trim();
        final String service = inputRecord.get("service").trim();
        String message;
        // If no service name is specified, just say "play <STATION>"
        // If it is specified, say "play <STATION> on <SERVICE>"
        if (service.isEmpty()) { 
            message = String.format("play %s", station);
        } else {
            message = String.format("play %s on %s", station, service);
        }
        JSONObject json = vd.message(message);
        System.out.println("Response JSON: " + json.toString());
        
        // Set all the fields on the output record - it containts everything from the CSV file, plus the actual values
        OutputRecord record = new OutputRecord();
        record.expectedImageURL = inputRecord.get("expectedImageURL").trim();
        record.expectedStreamURL = inputRecord.get("expectedStreamURL").trim();
        record.expectedTranscript = inputRecord.get("expectedTranscript").trim();
        record.service = inputRecord.get("service").trim();
        record.station = inputRecord.get("entity").trim();
        
        record.actualTranscript = json.getString("transcript");
        if (!json.isNull("streamURL")) {
            record.actualStreamURL = json.getString("streamURL");
        }
        // Use JSON Path to pull out the actual image URL
        // ReadContext ctx = JsonPath.parse(json.toString());
        // String imageURL = ctx.read("$.raw.messageBody.directives[4].payload.content.art.sources[0].url");
        // record.actualImageURL = imageURL;
        
        this.outputRecords.add(record);
        
        // Check that all our tests pass - we do partial matches between expected and actual fields
        record.success = this.contains(json.getString("transcript"), record.expectedTranscript);
        record.success &= this.contains(record.actualStreamURL, record.expectedStreamURL);
        // record.success &= this.contains(imageURL, record.expectedImageURL);

        // Send the results to DataDog
        DataDog dataDog = new DataDog(dotenv.get("DATA_DOG_API_KEY"));
        DataDog.Metric [] metrics = new DataDog.Metric[2];

        final String [] tags = new String [] {"station:" + record.station, "run:" + this.runName};
        metrics[0] = new DataDog.Metric("station_resolution.success", 
            "count", 
            record.success ? 1L : 0L,
            tags
        );
        metrics[1] = new DataDog.Metric("station_resolution.failure", 
            "count", 
            record.success ? 0L : 1L,
            tags
        );
        dataDog.postMetrics(metrics);
            
        assertTrue(record.success);
    }

    /**
     * Helper function for comparing actual and expected results via partial match logic
     */
    private boolean contains(String actual, String expected) {
        // Skip cases where the expected is blank
        if (expected.trim().isEmpty()) {
            return true;
        }

        try {
            assertThat(actual.trim().toLowerCase(), containsString(expected.toLowerCase()));
            return true;
        } catch (NullPointerException e) {
            return false;
        } catch (AssertionError e) {
            return false;
        }
    }

    /**
     * Prints out the results to a CSV file
     */
    private void printResults() {
        // Print the results to an output CSV
        String outputPath = System.getProperty("user.dir") + "/output.csv";
        try (CSVPrinter printer = new CSVPrinter(new FileWriter(outputPath), CSVFormat.EXCEL)) {
            // CSV headers go first
            printer.printRecord("STATION", 
                "SERVICE", 
                "EXPECTED TRANSCRIPT", 
                "EXPECTED STREAM URL", 
                "EXPECTED IMAGE URL",
                "ACTUAL TRANSCRIPT", 
                "ACTUAL STREAM URL",
                "ACTUAL IMAGE URL",
                "SUCCESS");
            for (OutputRecord record : this.outputRecords) {
                printer.printRecord(record.station, 
                    record.service, 
                    record.expectedTranscript, 
                    record.expectedStreamURL,
                    record.expectedImageURL,
                    record.actualTranscript,
                    record.actualStreamURL,
                    record.actualImageURL,
                    record.success
                );
            }
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static class OutputRecord {
        public String station;
        public String service;
        public String actualImageURL;
        public String actualStreamURL;
        public String actualTranscript;
        public String expectedImageURL;
        public String expectedStreamURL;
        public String expectedTranscript;
        public boolean success; 
    }
}
