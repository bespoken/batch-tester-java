# Setup

## Install The Java SDK
https://developers.redhat.com/products/openjdk/download

Be sure to install Java SDK 8 or above.

## Configuring the Virtual Device
Currently the system is setup to Emerson's Virtual Device.

For anything more than trivial tests, we highly recommend setting up your own virtual device and token.

This can be set in the `.env` file or it can be set as an environment variables (`VIRTUAL_DEVICE_TOKEN`).

# Running Tests
Tests are run with this command:
```
./mvn.sh test
```
It runs through all the utterances defined in stations.csv, checking to see that the expected fields match the actual fields. The columns in the stations CSV are:

| Field | Description | 
| --- | --- |
| entity | The station, show or movie to request |
| service | The service to test with (e.g., NBC, Hulu, etc.) |
| expectedTranscript | The expected transcript of the Alexa response - this is a partial match |
| expectedStreamURL | The expected stream URL of the Alexa response |
| expectedImageURL | The expected image for the x-small image |

## Test Output
The expected values are compared to the actual values for each entry in the CSV.

The output is then recorded as:
1) A Junit Assertion
2) An entry in the output csv file
3) Time-series data in DataDog

We create the results three ways to show how the assertions can be applied to different formats.

## Jenkins Configuration
We have setup a public Jenkins server for building this project.

URL: http://jenkins.bespoken.io  
Ask [support@bespoken.io](mailto:support@bespoken.io) for access.

There are two jobs configured:
* StationTest-Adhoc - station tests configured to run on an adhoc basis (i.e., by choosing "Build Now" inside Jenkins)
* StationTest-Scheduled - to be run every hour

The adhoc and scheduled job are nearly the same, except the adhoc one produces an email every time it runs. The schedule job does not, so as to avoid overloading the inbox.

You can view the build history for the jobs here:  
* [StationTest-Adhoc](http://jenkins.bespoken.io/jenkins/job/StationTest-Adhoc/)
* [StationTest-Scheduled](http://jenkins.bespoken.io/jenkins/job/StationTest-Scheduled/)


The Job is currently only run manually (by selecting "Build Now" inside Jenkins). For a full implementation, we would recommend setting up a regular schedule for it such as once a day.

When it concludes, it sends the build log as well as the CSV test results via email.

## DataDog Output
We send the following time series data for each test:  
* station_resolution.success - Set to 1 when the test succeeds, 0 when it fails
* station_resolution.failure - Set to 1 when the test fails, 0 when it succeeds

For each data point, we tag it with the station name as well as a run name.

The run name is just the time that tests were started in the format:  
`yyyy-MM-dd_HH-mm` - e.g., `2019-12-10_18-30`

We then turn this into graphs on DataDog, such as this:
<p align="center">
  <img src="static/DataDogDashboard.png" />
</p>
